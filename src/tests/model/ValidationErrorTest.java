/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import javax.swing.text.JTextComponent;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ValidationErrorTest {
    private ValidationError error;
    private List<JTextComponent> fields;

    @BeforeEach
    void setUp() {
        error = new ValidationError();
        fields = new ArrayList<>();
        fields.add(new JTextField("error1"));
        fields.add(new JTextField("error2"));
        fields.add(null);
        fields.add(new JTextField("error3"));
        fields.add(new JTextField("error4"));
        fields.add(null);
    }

    @Test
    void addError() {
        assertTrue(error.isValid());
        assertEquals(0, error.getErrorFields().size());
        error.addError(fields.get(0));
        error.addError(fields.get(1));
        error.addError(fields.get(2));
        error.addError(fields.get(3));
        error.setMessage("message about an error");

        assertFalse(error.isValid());
        assertEquals(3, error.getErrorFields().size());
        assertEquals("message about an error", error.getMessage());
    }

    @Test
    void addErrors() {
        assertTrue(error.isValid());
        assertEquals(0, error.getErrorFields().size());

        error.addErrors(null);
        assertTrue(error.isValid());
        assertEquals(0, error.getErrorFields().size());

        error.addErrors(fields);
        error.setMessage("message about an error");

        assertFalse(error.isValid());
        assertEquals(4, error.getErrorFields().size());
        assertEquals("message about an error", error.getMessage());
    }
}