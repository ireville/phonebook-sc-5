/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class ContactTableModelTest {
    private static Contact ira, merilyn, freddie, ozzy, rihanna;
    private ContactTableModel tableModel;

    @BeforeAll
    static void beforeAll() {
        ira = new Contact("Zyl", "Ira", "Vladimirovna", "1234",
                "5678", "Odi", LocalDate.now(), "notes");
        merilyn = new Contact("Monroe", "Merilyn", "", "431286",
                "87123", "NY", LocalDate.of(1926, 6, 1), "");
        freddie = new Contact("Mercury", "Freddie", "", "",
                "", "LA", LocalDate.of(1946, 9, 5), "Queen");
        ozzy = new Contact("Osbourne", "Ozzy", "", "666", "6666666666",
                "Odi", LocalDate.of(1948, 12, 3), "Black Sabbath");
        rihanna = new Contact("Robin", "Rihanna", "", "555",
                "342432", "Barbados", LocalDate.of(1988, 2, 20),
                "We like diamonds in the sky");
    }

    @BeforeEach
    void setUp() {
        tableModel = new ContactTableModel();

        tableModel.addContact(ira);
        tableModel.addContact(merilyn);
        tableModel.addContact(freddie);
        tableModel.addContact(ozzy);
        tableModel.addContact(rihanna);
    }

    @Test
    void isCellEditable() {
        assertFalse(tableModel.isCellEditable(0, 3));
        assertFalse(tableModel.isCellEditable(2, 2));
        assertFalse(tableModel.isCellEditable(1, 0));
        assertFalse(tableModel.isCellEditable(4, 3));
    }

    @Test
    void getRowCount() {
        assertEquals(5, tableModel.getRowCount());
        tableModel.setFilter("Merilyn");
        assertEquals(1, tableModel.getRowCount());

        tableModel = new ContactTableModel();
        assertEquals(0, tableModel.getRowCount());

        tableModel.addContact(new Contact());
        tableModel.addContact(new Contact(new String[]{"Zyl", "Ira", "Vladimirovna", "1234",
                "5678", "Odi", "2002-09-14", "notes"}));

        assertEquals(2, tableModel.getRowCount());
    }

    @Test
    void getColumnCount() {
        assertEquals(Contact.getFieldNames().size(), tableModel.getColumnCount());
        tableModel = new ContactTableModel();
        assertEquals(Contact.getFieldNames().size(), tableModel.getColumnCount());
    }

    @Test
    void getColumnName() {
        assertEquals("Фамилия", tableModel.getColumnName(0));
        assertEquals("Имя", tableModel.getColumnName(1));
        assertEquals("Отчество", tableModel.getColumnName(2));
        assertEquals("<html><center>Мобильный<br/>телефон</center></html>", tableModel.getColumnName(3));
        assertEquals("<html><center>Домашний<br/>телефон</center></html>", tableModel.getColumnName(4));
        assertEquals("Адрес", tableModel.getColumnName(5));
        assertEquals("<html><center>День<br/>Рождения</center></html>", tableModel.getColumnName(6));
        assertEquals("Комментарий", tableModel.getColumnName(7));
    }

    @Test
    void getValueAt() {
        assertEquals("Zyl", tableModel.getValueAt(0, 0));
        assertEquals("431286", tableModel.getValueAt(1, 3));
        assertEquals("6666666666", tableModel.getValueAt(3, 4));
        assertEquals("We like diamonds in the sky", tableModel.getValueAt(4, 7));

        tableModel.setFilter("Fre");
        assertEquals("Mercury", tableModel.getValueAt(0, 0));
    }

    @Test
    void getContact() {
        assertEquals(ira, tableModel.getContact(0));
        assertEquals(merilyn, tableModel.getContact(1));
        assertEquals(freddie, tableModel.getContact(2));
        assertEquals(ozzy, tableModel.getContact(3));
        assertEquals(rihanna, tableModel.getContact(4));

        tableModel.setFilter("y");
        assertEquals(ira, tableModel.getContact(0));
        assertEquals(ozzy, tableModel.getContact(3));
    }

    @Test
    void getContacts() {
        assertEquals(ira, tableModel.getContacts().get(0));
        assertEquals(merilyn, tableModel.getContacts().get(1));
        assertEquals(freddie, tableModel.getContacts().get(2));
        assertEquals(ozzy, tableModel.getContacts().get(3));
        assertEquals(rihanna, tableModel.getContacts().get(4));

        tableModel.setFilter("o");
        assertEquals(ira, tableModel.getContacts().get(0));
        assertEquals(ozzy, tableModel.getContacts().get(2));
    }

    @Test
    void getAllContacts() {
        assertEquals(ira, tableModel.getAllContacts().get(0));
        assertEquals(merilyn, tableModel.getAllContacts().get(1));
        assertEquals(freddie, tableModel.getAllContacts().get(2));
        assertEquals(ozzy, tableModel.getAllContacts().get(3));
        assertEquals(rihanna, tableModel.getAllContacts().get(4));

        tableModel.setFilter("o");
        assertEquals(ira, tableModel.getAllContacts().get(0));
        assertEquals(merilyn, tableModel.getAllContacts().get(1));

        tableModel.setFilter("i");
        assertEquals(ira, tableModel.getAllContacts().get(0));
        assertEquals(merilyn, tableModel.getAllContacts().get(1));
    }

    @Test
    void addContact() {
        int size = tableModel.getRowCount();
        tableModel.addContact(new Contact());
        tableModel.addContact(new Contact("Surname", "Name", "Patronymic"));
        tableModel.addContact(new Contact("new", "contact", "from add"));

        assertEquals(size + 3, tableModel.getRowCount());
        assertEquals("Surname", tableModel.getContacts().get(size + 1).getSurname());
        assertEquals("new", tableModel.getContacts().get(size + 2).getSurname());

        tableModel.setFilter("Surname");
        assertEquals(1, tableModel.getRowCount());
        assertEquals("Surname", tableModel.getContacts().get(0).getSurname());
    }

    @Test
    void editContact() {
        tableModel.getContact(0).setNotes("edited");
        tableModel.editContact(tableModel.getContact(0));
        assertEquals("edited", tableModel.getContact(0).getNotes());
        assertEquals("", tableModel.getContact(1).getNotes());
        assertEquals("We like diamonds in the sky", tableModel.getContact(4).getNotes());

        tableModel.getContact(3).setNotes(tableModel.getContact(3).getNotes() + " edited");
        tableModel.editContact(tableModel.getContact(3));
        tableModel.setFilter("zz");
        assertEquals("Black Sabbath edited", tableModel.getContact(0).getNotes());
    }

    @Test
    void removeContact() {
        tableModel.removeContact(merilyn);
        assertEquals(4, tableModel.getRowCount());
        assertEquals(freddie, tableModel.getContact(1));

        tableModel.setFilter("mer");
        assertEquals(0, tableModel.getRowCount());

        tableModel.setFilter("");
        tableModel.removeContact(ozzy);
        tableModel.removeContact(freddie);
        assertEquals(2, tableModel.getRowCount());
        assertEquals(ira, tableModel.getContact(0));
        assertEquals(rihanna, tableModel.getContact(1));

        tableModel.removeContact(ira);
        tableModel.removeContact(rihanna);
        assertEquals(0, tableModel.getRowCount());

        tableModel.removeContact(ira);
        tableModel.removeContact(rihanna);
        assertEquals(0, tableModel.getRowCount());
    }
}