/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package model;

import com.opencsv.exceptions.CsvValidationException;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FileWorkerTest {
    private static Contact ira, merilyn, freddie, ozzy, rihanna;
    private static File file;
    private ContactTableModel tableModel;

    @BeforeAll
    static void beforeAll() {
        ira = new Contact("Zyl", "Ira", "Vladimirovna", "1234",
                "5678", "Odi", null, "notes");
        merilyn = new Contact("Monroe", "Merilyn", "", "431286",
                "87123", "NY", LocalDate.of(1926, 6, 1), "");
        freddie = new Contact("Mercury", "Freddie", "", "",
                "", "LA", LocalDate.of(1946, 9, 5), "Queen");
        ozzy = new Contact("Osbourne", "Ozzy", "", "666", "6666666666",
                "Odi", LocalDate.of(1948, 12, 3), "Black Sabbath");
        rihanna = new Contact("Robin", "Rihanna", "", "555",
                "342432", "Barbados", LocalDate.of(1988, 2, 20),
                "We like diamonds in the sky");
    }

    @AfterAll
    static void tearDown() {
        assertTrue(file.delete());
    }

    @BeforeEach
    void setUp() {
        tableModel = new ContactTableModel();

        tableModel.addContact(ira);
        tableModel.addContact(merilyn);
        tableModel.addContact(freddie);
        tableModel.addContact(ozzy);
        tableModel.addContact(rihanna);

        file = new File("testFileWork.csv");
    }

    @Test
    void writeAndReadTable() {
        assertDoesNotThrow(() -> FileWorker.writeTableToFile(tableModel, file));
        assertDoesNotThrow(() -> FileWorker.readTableFromFile(file));

        List<String[]> contacts = new ArrayList<>();
        try {
            contacts = FileWorker.readTableFromFile(file);
        } catch (CsvValidationException | IOException e) {
            e.printStackTrace();
        }

        assertEquals("Zyl", contacts.get(0)[0]);
        assertEquals("Merilyn", contacts.get(1)[1]);
        assertEquals("LA", contacts.get(2)[5]);
        assertEquals("Osbourne", contacts.get(3)[0]);
        assertEquals("We like diamonds in the sky", contacts.get(4)[7]);
        assertEquals("", contacts.get(0)[6]);
        assertEquals("1926-06-01", contacts.get(1)[6]);
    }
}