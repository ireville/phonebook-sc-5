/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ContactTest {
    private Contact contact;

    @BeforeEach
    void setUp() {
        contact = new Contact("Surname", "Name", "Patronymic", "MobilePhone",
                "HomePhone", "Address", LocalDate.now(), "Some notes");
    }

    @Test
    void getFieldNames() {
        assertEquals("Фамилия", Contact.getFieldNames().get(0));
        assertEquals("Имя", Contact.getFieldNames().get(1));
        assertEquals("Отчество", Contact.getFieldNames().get(2));
        assertEquals("Мобильный телефон", Contact.getFieldNames().get(3));
        assertEquals("Домашний телефон", Contact.getFieldNames().get(4));
        assertEquals("Адрес", Contact.getFieldNames().get(5));
        assertEquals("День Рождения", Contact.getFieldNames().get(6));
        assertEquals("Комментарий", Contact.getFieldNames().get(7));
    }

    @Test
    void getFields() {
        assertEquals(contact.getSurname(), contact.getFields()[0]);
        assertEquals(contact.getName(), contact.getFields()[1]);
        assertEquals(contact.getPatronymic(), contact.getFields()[2]);
        assertEquals(contact.getMobilePhone(), contact.getFields()[3]);
        assertEquals(contact.getHomePhone(), contact.getFields()[4]);
        assertEquals(contact.getAddress(), contact.getFields()[5]);
        assertEquals(contact.getBirthDate(), contact.getFields()[6]);
        assertEquals(contact.getNotes(), contact.getFields()[7]);
    }

    @Test
    void getFieldsAsStrings() {
        assertEquals(contact.getSurname(), contact.getFieldsAsStrings()[0]);
        assertEquals(contact.getName(), contact.getFieldsAsStrings()[1]);
        assertEquals(contact.getPatronymic(), contact.getFieldsAsStrings()[2]);
        assertEquals(contact.getMobilePhone(), contact.getFieldsAsStrings()[3]);
        assertEquals(contact.getHomePhone(), contact.getFieldsAsStrings()[4]);
        assertEquals(contact.getAddress(), contact.getFieldsAsStrings()[5]);
        assertEquals(contact.getBirthDate().toString(), contact.getFieldsAsStrings()[6]);
        assertEquals(contact.getNotes(), contact.getFieldsAsStrings()[7]);
        assertEquals(new Contact().getNotes(), new Contact().getFieldsAsStrings()[7]);
    }

    @Test
    void updateContactInfo() {
        List<JTextComponent> textFields = new ArrayList<>();

        for (int i = 0; i < 8; i++) {
            textFields.add(new JTextField(i + ""));
        }

        contact.updateContactInfo(textFields, LocalDate.now());

        assertEquals("0", contact.getSurname());
        assertEquals("1", contact.getName());
        assertEquals("3", contact.getMobilePhone());
        assertEquals("5", contact.getAddress());
        assertEquals("6", contact.getNotes());
    }

    @Test
    void updateContactImportInfo() {
        Contact contact2 = new Contact(new String[]{"Surname2", "Name2", "Patronymic2",
                "MobilePhone2", "HomePhone2", "Address2", "2020-09-09",
                "Some notes2"});

        contact.updateContactImportInfo(contact2);
        assertEquals(contact2.getMobilePhone(), contact.getMobilePhone());
        assertEquals(contact2.getHomePhone(), contact.getHomePhone());
        assertEquals(contact2.getAddress(), contact.getAddress());
        assertEquals(contact2.getNotes(), contact.getNotes());
    }

    @Test
    void hashCodeTest() {
        Contact contact = new Contact("Surname", "Name", "Patronymic");
        Contact contact2 = new Contact(new String[]{"Surname", "Name", "Patronymic", "1234",
                "5678", "Odi", "2002-09-14", "notes"});

        assertEquals(contact.hashCode(), contact2.hashCode());
        assertEquals(contact, contact2);
        assertEquals(contact, contact);

        //noinspection AssertBetweenInconvertibleTypes
        assertNotEquals(contact, new String[]{""});
    }
}