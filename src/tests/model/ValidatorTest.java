/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package model;

import org.junit.jupiter.api.Test;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ValidatorTest {
    @Test
    void validateTelephone() {
        assertTrue(Validator.validateTelephone("182731293"));
        assertTrue(Validator.validateTelephone("01237123982"));
        assertTrue(Validator.validateTelephone("129370132712839"));
        assertFalse(Validator.validateTelephone("182tears21"));
        assertFalse(Validator.validateTelephone("something"));
        assertFalse(Validator.validateTelephone("hello"));
        assertFalse(Validator.validateTelephone("+192398just_text"));
        assertFalse(Validator.validateTelephone("+19823912"));
        assertFalse(Validator.validateTelephone("sdl--12-3412"));
        assertFalse(Validator.validateTelephone("-1879131"));
        assertFalse(Validator.validateTelephone("123-123-123124-24"));
        assertTrue(Validator.validateTelephone(""));
        assertTrue(Validator.validateTelephone("       "));
        assertTrue(Validator.validateTelephone("192730123"));
        assertTrue(Validator.validateTelephone(""));
    }

    @Test
    void validateDate() {
        assertTrue(Validator.validateDate(LocalDate.of(1999, 1, 1)));
        assertTrue(Validator.validateDate(LocalDate.of(1, 2, 3)));
        assertTrue(Validator.validateDate(LocalDate.of(1274, 4, 12)));
        assertTrue(Validator.validateDate(LocalDate.of(2010, 12, 29)));
        assertTrue(Validator.validateDate(LocalDate.of(2019, 10, 9)));
        assertTrue(Validator.validateDate(LocalDate.now()));
        assertFalse(Validator.validateDate(LocalDate.of(2030, 12, 12)));
        assertFalse(Validator.validateDate(LocalDate.of(2837, 8, 7)));
        assertFalse(Validator.validateDate(LocalDate.of(2040, 6, 24)));
        assertFalse(Validator.validateDate(LocalDate.of(2500, 9, 16)));
    }

    @Test
    void validateFormTrue() {
        List<JTextComponent> fields = List.of(new JTextField("Surname"), new JTextField("Name"),
                new JTextField("Patronymic"), new JTextField("123"), new JTextField("321"), new JTextField("Address"),
                new JTextField("Notes"));
        assertTrue(Validator.validateForm(fields, LocalDate.now()).isValid());

        fields = List.of(new JTextField("Surname"), new JTextField("Name"),
                new JTextField("Patronymic"), new JTextField(""), new JTextField("321"), new JTextField("Address"),
                new JTextField("Notes"));
        assertTrue(Validator.validateForm(fields, LocalDate.of(1287, 12, 29)).isValid());

        fields = List.of(new JTextField("Surname"), new JTextField("Name"),
                new JTextField(""), new JTextField(""), new JTextField("321"), new JTextField("Address"),
                new JTextField("Surname"));
        assertTrue(Validator.validateForm(fields, LocalDate.of(1999, 7, 7)).isValid());

        fields = List.of(new JTextField("Surname"), new JTextField("Name"),
                new JTextField(""), new JTextField(""), new JTextField("321"), new JTextField(""), new JTextField(""));
        assertTrue(Validator.validateForm(fields, LocalDate.of(2016, 2, 25)).isValid());
    }

    @Test
    void validateFormFalse() {
        List<JTextComponent> fields = List.of(new JTextField("Surname"), new JTextField(""),
                new JTextField(""), new JTextField(""), new JTextField("321"), new JTextField(""), new JTextArea(""));
        ValidationError error = Validator.validateForm(fields, LocalDate.of(2016, 2, 25));
        assertFalse(error.isValid());
        assertEquals(1, error.getErrorFields().size());

        fields = List.of(new JTextField(""), new JTextField(""),
                new JTextField(""), new JTextField(""), new JTextField("321"), new JTextField(""), new JTextArea(""));
        error = Validator.validateForm(fields, LocalDate.of(2017, 1, 27));
        assertFalse(error.isValid());
        assertEquals(2, error.getErrorFields().size());

        fields = List.of(new JTextField("Surname"), new JTextField("Name"),
                new JTextField(""), new JTextField(""), new JTextField("bye"), new JTextField(""), new JTextArea(""));
        error = Validator.validateForm(fields, LocalDate.of(2020, 2, 25));
        assertFalse(error.isValid());
        assertEquals(1, error.getErrorFields().size());

        fields = List.of(new JTextField(""), new JTextField(""),
                new JTextField(""), new JTextField(""), new JTextField(""), new JTextField(""), new JTextArea(""));
        error = Validator.validateForm(fields, LocalDate.of(2020, 2, 25));
        assertFalse(error.isValid());
        assertEquals(4, error.getErrorFields().size());

        fields = List.of(new JTextField("Surname"), new JTextField("Name"),
                new JTextField(""), new JTextField("sdf"), new JTextField("321"), new JTextField(""), new JTextArea(""));
        error = Validator.validateForm(fields, LocalDate.of(2021, 12, 25));
        assertFalse(error.isValid());
        assertEquals(1, error.getErrorFields().size());
        assertEquals("Рождение в будущем невозможно.\nНекорректная дата.", error.getMessage());

        fields = List.of(new JTextField(""), new JTextField(""),
                new JTextField(""), new JTextField(""), new JTextField(""), new JTextField(""), new JTextArea(""));
        error = Validator.validateForm(fields, LocalDate.of(2030, 2, 5));
        assertFalse(error.isValid());
        assertEquals(4, error.getErrorFields().size());
        assertEquals("Рождение в будущем невозможно.\nНекорректная дата.", error.getMessage());


        fields = List.of(new JTextField("<save me from html>"), new JTextField("something"),
                new JTextField(""), new JTextField("123"), new JTextField(""), new JTextField(""), new JTextArea(""));
        error = Validator.validateForm(fields, LocalDate.now());
        assertFalse(error.isValid());
        assertEquals("Ни одно поле не может содержать символ '<'.", error.getMessage());
    }

    @Test
    void validateContactLine() {
        assertTrue(Validator.validateContactLine(new String[]{"Surname", "Name", "Patronymic", "123", "321", "Address",
                "2021-03-03", "Notes"}));
        assertTrue(Validator.validateContactLine(new String[]{"Surname", "Name", "Patronymic", "", "321", "Address",
                "2021-03-03", ""}));
        assertTrue(Validator.validateContactLine(new String[]{"Surname", "Name", "Patronymic", "123", "", "",
                "", ""}));
        assertTrue(Validator.validateContactLine(new String[]{"Surname", "Name", "", "", "321", "Address",
                "2021-03-03", "Notes"}));
        assertTrue(Validator.validateContactLine(new String[]{"Surname", "Name", "", "123", "", "", "", ""}));


        assertFalse(Validator.validateContactLine(new String[]{"", "Name", "Patronymic", "123", "321", "Address",
                "2021-03-03", "Notes"}));
        assertFalse(Validator.validateContactLine(new String[]{"Surname", "", "Patronymic", "123", "321", "Address",
                "2021-03-03", "Notes"}));
        assertFalse(Validator.validateContactLine(new String[]{"", "", "Patronymic", "123", "321", "Address",
                "2020-05-01", "Notes"}));
        assertFalse(Validator.validateContactLine(new String[]{"Surname", "Name", "Patronymic", "", "", "Address",
                "2021-03-03", "Notes"}));

        assertFalse(Validator.validateContactLine(new String[]{"Surname", "Name", "Patronymic", "", "", "Address",
                "2022-03-03", "Notes"}));
        assertFalse(Validator.validateContactLine(new String[]{"Surname", "Name", "Patronymic", "", "", "Address",
                "3000-01-01", "Notes"}));


        assertFalse(Validator.validateContactLine(new String[]{"", "", "", "", "", "Address", "", ""}));
        assertFalse(Validator.validateContactLine(new String[]{"", "", "", "567", "", "Address", "", "hi"}));
        assertFalse(Validator.validateContactLine(new String[]{"Surname", "Name", "", "", "", "Address", "", ""}));

        assertFalse(Validator.validateContactLine(new String[]{"Surname", "Name", "", "sfd", "", "Address", "", ""}));
        assertFalse(Validator.validateContactLine(new String[]{"Surname", "Name", "", "", "hello", "Address", "", ""}));
        assertFalse(Validator.validateContactLine(new String[]{"Surname", "Name", "", "", "12-21", "Address", "", ""}));
        assertFalse(Validator.validateContactLine(new String[]{"Surname", "Name", "", "+228349", "hello", "Address",
                "", ""}));

        assertFalse(Validator.validateContactLine(new String[]{"Surname", "Name", "", "", "122", "Address",
                "1234-12-00", ""}));
        assertFalse(Validator.validateContactLine(new String[]{"Surname", "Name", "", "", "432", "Address",
                "hello", ""}));
        assertFalse(Validator.validateContactLine(new String[]{"Surname", "Name", "", "912837918372", "102", "Address",
                "1823927381", ""}));


        assertFalse(Validator.validateContactLine(null));
        assertFalse(Validator.validateContactLine(new String[]{"Surname", null, "", "", "432", "Address", null, ""}));
        assertFalse(Validator.validateContactLine(new String[]{"Surname", "Name", ""}));
        assertFalse(Validator.validateContactLine(new String[]{"Surname", "Name", "<hello"}));
        assertFalse(Validator.validateContactLine(new String[]{"<save me from html>", "Name", "hello"}));
    }

    @Test
    void checkPresence() {
        ContactTableModel tableModel = new ContactTableModel();
        tableModel.addContact(new Contact("Surname1", "Name1", "Patronymic1"));
        tableModel.addContact(new Contact("Surname2", "Name2", "Patronymic2"));
        tableModel.addContact(new Contact("Surname3", "Name3", "Patronymic3"));
        tableModel.addContact(new Contact("Surname4", "Name4", "Patronymic4"));

        assertTrue(Validator.checkPresence(tableModel,
                new Contact("Surname1", "Name1", "Patronymic1")) != -1);
        assertTrue(Validator.checkPresence(tableModel,
                new Contact("Surname2", "Name2", "Patronymic2")) != -1);
        assertTrue(Validator.checkPresence(tableModel,
                new Contact("Surname3", "Name3", "Patronymic3")) != -1);
        assertTrue(Validator.checkPresence(tableModel,
                new Contact("Surname4", "Name4", "Patronymic4")) != -1);

        assertFalse(Validator.checkPresence(tableModel,
                new Contact("Surname", "Name", "Patronymic")) != -1);
        assertFalse(Validator.checkPresence(tableModel,
                new Contact("Zyl", "Ira", "")) != -1);
        assertFalse(Validator.checkPresence(tableModel,
                new Contact("OtherSurname", "OtherName", "OtherPatronymic")) != -1);
        assertFalse(Validator.checkPresence(tableModel,
                new Contact("OtherOtherSurname", "OtherOtherName", "")) != -1);

        assertTrue(Validator.checkPresence(tableModel, tableModel.getContact(0)) != -1);
        assertTrue(Validator.checkPresence(tableModel, tableModel.getContact(1)) != -1);
        assertTrue(Validator.checkPresence(tableModel, tableModel.getContact(2)) != -1);
        assertTrue(Validator.checkPresence(tableModel, tableModel.getContact(3)) != -1);
    }
}