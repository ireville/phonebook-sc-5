/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package view;

import model.Contact;

import javax.swing.*;
import java.awt.*;
import java.io.File;

public class MessageDialogs {
    /**
     * Предупреждение о некорректном формате файла.
     *
     * @param parent Окно-родитель.
     * @return Окно-предупреждение.
     */
    public static int showWrongFileFormatDialog(JComponent parent) {
        Object[] options = {"Отмена", "Да"};
        return JOptionPane.showOptionDialog(parent,
                "Внимание! Формат выбранного файла - не .csv." +
                        "\nМожет возникнуть ошибка работы с файлом. \nВы уверены в выборе?",
                "Требуется файл .csv",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null,
                options,
                options[0]);
    }

    /**
     * Предупреждение о перезаписи файла.
     *
     * @param parent     Окно-родитель.
     * @param chosenFile Выбранный файл.
     * @return Окно-предупреждение.
     */
    public static int showFileEnsureDialog(JComponent parent, File chosenFile) {
        Object[] options = {"Отмена", "Да"};
        return JOptionPane.showOptionDialog(parent,
                "Внимание! \nВы выбрали файл:" + chosenFile.getAbsolutePath() +
                        "\nОн будет перезаписан. Информация, хранящаяся в нём сейчас, будет утеряна.\n" +
                        "Вы уверены, что выбрали нужный файл?", "Подтверждение выбора файла экспорта",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null,
                options,
                options[0]);
    }

    /**
     * Информирование об ошибке работы с файлом при экспорте.
     *
     * @param parent Окно-родитель.
     * @param file   Файл экспорта.
     */
    public static void showExportErrorDialog(JComponent parent, File file) {
        JOptionPane.showMessageDialog(parent,
                "Произошла ошибка экспорта контактов в файл" + file.getAbsolutePath(),
                "Ошибка записи в файл",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Информирование об ошибке работы с файлом при импорте.
     *
     * @param parent Окно-родитель.
     * @param file   Файл импорта.
     */
    public static void showImportFileErrorDialog(JComponent parent, File file) {
        JOptionPane.showMessageDialog(parent,
                "Произошла ошибка чтения файла: \n" + file.getAbsolutePath(),
                "Ошибка импорта",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Информирование об ошибке типа данных в файле импорта.
     *
     * @param parent Окно-родитель.
     * @param file   Файл импорта.
     */
    public static void showImportFormatErrorDialog(JComponent parent, File file) {
        JOptionPane.showMessageDialog(parent,
                "Неверный формат данных в файле: \n" + file.getAbsolutePath(),
                "Ошибка импорта",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Информирование об ошибке работы с файлом при сохранении данных.
     *
     * @param parent Окно-родитель.
     */
    public static void showSaveErrorDialog(JFrame parent) {
        JOptionPane.showMessageDialog(parent,
                "Произошла ошибка сохранения данных телефонной книги",
                "Ошибка записи в файл",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Информирование о наличии некорректных контактов в импортируемых данных.
     *
     * @param parent Окно-родитель.
     */
    public static void showIncorrectImportTableDialog(JComponent parent) {
        JOptionPane.showMessageDialog(parent,
                "В считанном файле содержатся некорректные контакты.\n" +
                        "Такая телефонная книга считается недопустимой и импортироваться не будет.",
                "Ошибка импорта",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Предупреждение о наличии такого же контакта и перезаписи в таблице при редактировании/добавлении.
     *
     * @param parent Окно-родитель.
     * @return Окно с выбором путей решения.
     */
    public static int showSameContactDialog(JDialog parent) {
        Object[] options = {"Отмена", "Да"};
        return JOptionPane.showOptionDialog(parent,
                "Контакт с такой Фимилией, Именем и Отчеством уже существует.\n" +
                        "Хотите перезаписать данные контакта на текущие?", "Подтверждение перезаписи контакта",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null,
                options,
                options[0]);
    }

    /**
     * Предупреждение о наличии такого же контакта в таблице при импорте.
     *
     * @param parent          Окно-родитель.
     * @param conflictContact Контакт, вызвавший конфликт.
     * @return Окно с выбором путей решения.
     */
    public static int showSameContactImportDialog(Component parent, Contact conflictContact) {
        Object[] options = {"Пропустить", "Заменить", "Пропустить для всех", "Заменить для всех"};
        return JOptionPane.showOptionDialog(parent,
                "Контакт: " + conflictContact.getSurname() + " " + conflictContact.getName() + " " +
                        conflictContact.getPatronymic() +
                        "\nУже есть в телефонной книге. Хотите заменить его на импортируемый?\n",
                "Совпадение контакта",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null,
                options,
                options[0]);
    }

    /**
     * Подтверждение об удалении контакта.
     *
     * @param parent Окно-родитель.
     * @return Окно-подтверждение.
     */
    public static int showEnsureRemoveContactDialog(JFrame parent) {
        Object[] options = {"Отмена", "Да"};
        return JOptionPane.showOptionDialog(parent,
                "Вы уверены, что хотите удалить этот контакт?", "Подтверждение удаления",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null,
                options,
                options[0]);
    }

    /**
     * Окно с информацией о разработчике.
     *
     * @param parent Окно-родитель.
     */
    public static void showMyInfoDialog(JFrame parent) {
        JOptionPane.showMessageDialog(parent,
                "Это великолепное приложение разработала\nЗыль Ирина Владимировна\nГруппа: БПИ192\n" +
                        "Послание потомкам: Крепитесь, кушайте печеньки\nи пользуйтесь моей телефонной книгой, а то\n" +
                        "СКЛЕРОЗ НЕ ЗА ГОРАМИ.",
                "Информация о разработчике",
                JOptionPane.INFORMATION_MESSAGE);
    }
}
