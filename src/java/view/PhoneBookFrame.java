/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package view;

import controller.AddContactController;
import controller.EditContactController;
import controller.PhoneBookController;
import controller.RemoveContactController;
import model.ContactTableModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;

public class PhoneBookFrame extends JFrame {
    // Путь к файлу для дефолтного сохранения таблицы при выходе.
    private static final String FILE_PATH = "phonebook.csv";

    public PhoneBookFrame() {
        super("PhoneBook");

        // Задаём стиль.
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ignored) {
        }

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int screenHeight = screenSize.height;
        int screenWidth = screenSize.width;

        setPreferredSize(new Dimension(screenWidth / 2, screenHeight / 2));
        setLocation(screenWidth / 5, screenHeight / 5);
        setMinimumSize(new Dimension(screenWidth / 2, screenHeight / 2));

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        Container contentPane = getContentPane();

        // Создаём таблицу.
        PhoneBookTable table = new PhoneBookTable();
        ContactTableModel tableModel = (ContactTableModel) table.getModel();
        table.setRowHeight(22);
        table.setFont(new Font("Times New Roman", Font.PLAIN, 18));

        // Создаём верхнее и нижнее меню.
        JMenuBar upperMenu = new UpperMenu(table, tableModel, this);
        JPanel lowerMenu = createLowerMenu(this, table, tableModel);

        // Добавляем в панель.
        contentPane.add(upperMenu, BorderLayout.NORTH);
        contentPane.add(new JScrollPane(table), BorderLayout.CENTER);
        contentPane.add(lowerMenu, BorderLayout.SOUTH);

        this.addWindowListener(PhoneBookController.closeFrameListener(this, tableModel, FILE_PATH));
    }

    /**
     * Создаёт нижнее меню с кнопками "Удалить", "Редактировать" и "Добавить" контакт, а также поисковой строкой и
     * кнопкой "Поиск".
     *
     * @param parent     Окно-родитель
     * @param table      Таблица.
     * @param tableModel Модель таблицы контактов.
     * @return Созданная панель.
     */
    public JPanel createLowerMenu(JFrame parent, JTable table, ContactTableModel tableModel) {
        JPanel lowerMenu = new JPanel();

        // Создаём и добавляем кнопки.
        JButton removeButton = new JButton("Удалить");
        removeButton.addActionListener(RemoveContactController.removeContactButtonListener(parent, table, tableModel));

        JButton editButton = new JButton("Редактировать");
        editButton.addActionListener(EditContactController.createEditFormButtonListener(parent, table, tableModel));

        JButton addButton = new JButton("Добавить");
        addButton.addActionListener(AddContactController.createAddFormButtonListener(parent, tableModel));

        lowerMenu.add(removeButton);
        lowerMenu.add(editButton);
        lowerMenu.add(addButton);

        // Создаём и добавляем поисковую строку и кнопку поиска.
        JTextField searchLine = new JTextField();
        searchLine.addKeyListener(PhoneBookController.searchLineListener(tableModel));

        JButton searchButton = new JButton("Поиск");
        searchButton.addActionListener(PhoneBookController.searchButtonListener(tableModel, searchLine));

        lowerMenu.add(searchLine);
        lowerMenu.add(searchButton);

        lowerMenu.setLayout(new BoxLayout(lowerMenu, BoxLayout.X_AXIS));

        return lowerMenu;
    }

    /**
     * Функция, вызываемая для закрытия окна.
     */
    public void close() {
        processWindowEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }
}
