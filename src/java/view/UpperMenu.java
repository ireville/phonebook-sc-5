/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package view;

import controller.*;
import model.ContactTableModel;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;

public class UpperMenu extends JMenuBar {
    private final JTable table;
    private final ContactTableModel tableModel;
    private final JFrame parent;

    public UpperMenu(JTable table, AbstractTableModel tableModel, JFrame parent) {
        this.table = table;
        this.tableModel = (ContactTableModel) tableModel;
        this.parent = parent;

        // Создаем все пункты верхнего меню.
        JMenu fileMenu = createFileMenu();
        JMenu settingsMenu = createSettingsMenu();
        JMenu aboutMenu = createAboutMenu();

        setLayout(new FlowLayout(FlowLayout.LEFT));
        add(fileMenu);
        add(settingsMenu);
        add(aboutMenu);
    }

    /**
     * Создаёт выпадающий список для вкладки "Файл".
     *
     * @return Вкладка "Файл".
     */
    private JMenu createFileMenu() {
        JMenu fileMenu = new JMenu("Файл");

        JMenuItem addItem = new JMenuItem("Добавить");
        addItem.addActionListener(AddContactController.createAddFormButtonListener(parent, tableModel));

        JMenuItem editItem = new JMenuItem("Редактировать");
        editItem.addActionListener(EditContactController.createEditFormButtonListener(parent, table, tableModel));

        JMenuItem removeItem = new JMenuItem("Удалить");
        removeItem.addActionListener(RemoveContactController.removeContactButtonListener(parent, table, tableModel));

        JMenuItem exitItem = new JMenuItem("Выход");
        exitItem.addActionListener(PhoneBookController.closeFrameButtonListener((PhoneBookFrame) parent));

        fileMenu.add(addItem);
        fileMenu.add(editItem);
        fileMenu.add(removeItem);
        fileMenu.add(exitItem);

        return fileMenu;
    }

    /**
     * Создаёт выпадающий список для вкладки "Настройки".
     *
     * @return Вкладка "Настройки".
     */
    private JMenu createSettingsMenu() {
        JMenu settingsMenu = new JMenu("Настройки");

        JMenuItem importItem = new JMenuItem("Импортировать");
        importItem.addActionListener(FileWorkController.importFileListener(this, tableModel));

        JMenuItem exportItem = new JMenuItem("Экспортировать");
        exportItem.addActionListener(FileWorkController.exportFileListener(this, tableModel));

        settingsMenu.add(importItem);
        settingsMenu.add(exportItem);

        return settingsMenu;
    }

    /**
     * Создаёт выпадающий список для вкладки "Спрвка".
     *
     * @return Вкладка "Справка".
     */
    public JMenu createAboutMenu() {
        JMenu aboutMenu = new JMenu("Справка");

        JMenuItem devItem = new JMenuItem("О разработчике");
        devItem.addActionListener(PhoneBookController.aboutButtonListener(parent));

        aboutMenu.add(devItem);

        return aboutMenu;
    }
}
