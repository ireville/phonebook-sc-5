/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package view;

import model.Contact;
import model.ContactTableModel;

import javax.swing.*;
import java.util.List;
import java.util.stream.Collectors;

public class PhoneBookTable extends JTable {
    public PhoneBookTable() {
        super(new ContactTableModel());
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        getColumnModel().setColumnSelectionAllowed(false);
        getTableHeader().setReorderingAllowed(false);
        getTableHeader().setResizingAllowed(false);
    }

    /**
     * Создает названия для колонок таблицы, опираясь на названия полей контакта.
     *
     * @return Названия колонок (хэдер таблицы).
     */
    public static List<String> getColumnNames() {
        return Contact.getFieldNames().stream().map(name -> {
            if (name.contains(" ")) {
                return "<html><center>" + name.replace(" ", "<br/>") + "</center></html>";
            } else {
                return name;
            }
        }).collect(Collectors.toList());
    }
}
