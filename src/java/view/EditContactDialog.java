/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package view;

import controller.AddContactController;
import controller.EditContactController;
import model.Contact;
import model.ContactTableModel;
import org.jdatepicker.impl.DateComponentFormatter;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class EditContactDialog extends JDialog {
    private final ContactTableModel tableModel;

    // Количество текстовых полей для ввода.
    private final int fieldsCnt;
    private List<JTextComponent> textFields;
    private JDatePickerImpl datePicker;

    public EditContactDialog(Frame parent, AbstractTableModel tableModel, Contact editedContact, String flag) {
        super(parent, "", true);
        fieldsCnt = Contact.getFieldNames().size() - 1;
        this.tableModel = (ContactTableModel) tableModel;

        setMinimumSize(new Dimension(Toolkit.getDefaultToolkit().getScreenSize().width / 3,
                Toolkit.getDefaultToolkit().getScreenSize().height / 2));


        JPanel gridMenu = createMainGridPanel(editedContact, 20, 0, 5);
        JPanel buttonMenu = createButtonMenu(this, editedContact, flag);

        JPanel menu = new JPanel();
        menu.setLayout(new BoxLayout(menu, BoxLayout.Y_AXIS));
        menu.add(gridMenu);
        menu.add(Box.createRigidArea(new Dimension(0, 20)));
        menu.add(textFields.get(textFields.size() - 1));

        getContentPane().add(menu, BorderLayout.CENTER);
        getContentPane().add(buttonMenu, BorderLayout.SOUTH);

        this.pack();
    }

    /**
     * Создаёт текстовое поля для ввода данных.
     *
     * @param fieldText   Изначальный текст, отображаемый в поле.
     * @param fieldLength Длина текстового поля.
     * @param fieldName   Наименование поля.
     * @return Созданное поле.
     */
    public JTextField createInsertField(String fieldText, int fieldLength, String fieldName) {
        JTextField textField = new JTextField(fieldText, fieldLength);
        textField.setFont(new Font("Times New Roman", Font.PLAIN, 18));
        textField.setToolTipText(fieldName);

        return textField;
    }

    /**
     * Создаёт текстовые поля для ввода данных.
     *
     * @param editedContact Изменяемый контакт.
     * @param fieldLength   Длина текстового поля.
     * @return Созданные ткстовые поля.
     */
    public java.util.List<JTextComponent> createInsertFields(Contact editedContact, int fieldLength) {
        List<JTextComponent> textFields = new ArrayList<>();
        for (int i = 0; i < fieldsCnt - 1; i++) {
            textFields.add(createInsertField((String) editedContact.getFields()[i], fieldLength,
                    Contact.getFieldNames().get(i)));
        }

        JTextArea area = new JTextArea((String) editedContact.getFields()[fieldsCnt]);
        area.setFont(new Font("Times New Roman", Font.PLAIN, 18));
        area.setToolTipText(Contact.getFieldNames().get(fieldsCnt));

        textFields.add(area);

        return textFields;
    }

    /**
     * Создаёт основное меню с именами полей для ввода и самими полями для ввода.
     *
     * @param editedContact Изменяемый/Добавляемый контакт.
     * @param fieldsLength  Длина полей ввода текста.
     * @param hGap          Горизонтальный отступ грида.
     * @param vGap          Верикальный отступ грида.
     * @return Созданная панель.
     */
    public JPanel createMainGridPanel(Contact editedContact, int fieldsLength, int hGap, int vGap) {
        // Создаём поля для ввода/выбора данных.
        textFields = createInsertFields(editedContact, fieldsLength);
        datePicker = createDatePicker(editedContact);

        JPanel gridMenu = new JPanel(new GridLayout(textFields.size(), 2, hGap, vGap));
        JLabel label;

        for (int i = 0; i < textFields.size(); i++) {
            label = new JLabel(Contact.getFieldNames().get(i));
            label.setFont(new Font("Times New Roman", Font.BOLD, 20));
            label.setHorizontalAlignment(JLabel.CENTER);
            gridMenu.add(label);

            if (i == textFields.size() - 1) {
                gridMenu.add(datePicker);
            } else {
                gridMenu.add(textFields.get(i));
            }
        }


        return gridMenu;
    }

    /**
     * Создаёт панель с кнопками "Сохранить" и "Отмена".
     *
     * @param parent        Родительское окно, в котором будет располагаться меню.
     * @param editedContact Изменяемый контакт.
     * @param flag          Флаг создания или изменения контакта.
     * @return Созданная панель.
     */
    public JPanel createButtonMenu(JDialog parent, Contact editedContact, String flag) {
        JPanel buttonMenu = new JPanel();

        JButton cancelButton = new JButton("Отмена");
        cancelButton.addActionListener(EditContactController.cancelEditFormButtonListener(this));

        JButton saveButton = new JButton("Сохранить");
        if (flag.equals("Edit")) {
            saveButton.addActionListener(EditContactController.saveEditFormButtonListener(parent, tableModel,
                    editedContact, textFields, datePicker));
        } else if (flag.equals("Add")) {
            saveButton.addActionListener(AddContactController.saveAddFormButtonListener(parent, tableModel,
                    editedContact, textFields, datePicker));
        }


        buttonMenu.add(cancelButton);
        buttonMenu.add(saveButton);
        buttonMenu.setLayout(new FlowLayout(FlowLayout.RIGHT));

        return buttonMenu;
    }

    /**
     * Создаёт поле для выбора даты.
     *
     * @param editedContact Изменяемый контакт.
     * @return Поле для выбора даты.
     */
    public JDatePickerImpl createDatePicker(Contact editedContact) {
        // Модель даты.
        UtilDateModel uiModel = new UtilDateModel();

        if (editedContact.getBirthDate() != null) {
            uiModel.setDate(editedContact.getBirthDate().getYear(),
                    editedContact.getBirthDate().getMonthValue() - 1,
                    editedContact.getBirthDate().getDayOfMonth());
            uiModel.setSelected(true);
        }

        // Панель даты.
        JDatePanelImpl datePanel = new JDatePanelImpl(uiModel, new Properties());

        return new JDatePickerImpl(datePanel, new DateComponentFormatter());
    }
}
