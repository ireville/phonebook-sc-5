/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package controller;

import model.Contact;
import model.ContactTableModel;
import model.ValidationError;
import model.Validator;
import org.jdatepicker.impl.JDatePickerImpl;
import view.EditContactDialog;
import view.MessageDialogs;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.List;

public class AddContactController {
    /**
     * Реакция на нажатие кнопки добавления контакта: создаёт окно добавления контакта.
     *
     * @param parent     Окно-родитель.
     * @param tableModel Модель таблицы контактов.
     * @return Реакция на событие.
     */
    public static ActionListener createAddFormButtonListener(JFrame parent, ContactTableModel tableModel) {
        return e -> {
            Contact newContact = new Contact();
            JDialog editFrame = new EditContactDialog(parent, tableModel, newContact, "Add");
            editFrame.setVisible(true);
        };
    }

    /**
     * Реакция на нажатие кнопки "Сохранить" формы добавления контакта.
     *
     * @param parent       Окно-родитель.
     * @param tableModel   Модель таблицы контактов.
     * @param addedContact Добавляемый контакт.
     * @param textFields   Поля ввода.
     * @param datePicker   Поле выбора даты.
     * @return Реакция на событие.
     */
    public static ActionListener saveAddFormButtonListener(JDialog parent, ContactTableModel tableModel,
                                                           Contact addedContact, List<JTextComponent> textFields,
                                                           JDatePickerImpl datePicker) {
        return e -> {
            LocalDate date = EditContactController.fillDate(datePicker);

            ValidationError error = Validator.validateForm(textFields, date);
            EditContactController.changeErrorFields(error, textFields);

            if (!error.isValid()) {
                JOptionPane.showMessageDialog(parent, error.getMessage(), "Ошибка добавления",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }

            if (DuplicateController.checkAndSolveDuplicateFormContact(parent, tableModel, textFields, date) == 1) {
                return;
            }

            JOptionPane.showMessageDialog(parent, "Данные сохранены!");
            addedContact.updateContactInfo(textFields, date);
            tableModel.addContact(addedContact);
            parent.dispose();
        };
    }

    /**
     * Производит импорт контактов.
     *
     * @param parent     Компонент-родитель.
     * @param tableModel Модель таблицы контактов.
     * @param lineList   Список импортируемых контактов.
     */
    public static void organizeContactImport(Component parent, ContactTableModel tableModel, List<String[]> lineList) {
        tableModel.setFilter("");

        int presence;
        boolean allOld = false, allNew = false;

        for (String[] line : lineList) {
            Contact newContact = new Contact(line);
            presence = Validator.checkPresence(tableModel, newContact);
            if (presence != -1) {
                if (!allOld && !allNew) {
                    int chosen = MessageDialogs.showSameContactImportDialog(parent, newContact);
                    switch (chosen) {
                        case 2:
                            allOld = true;
                            break;
                        case 3:
                            allNew = true;
                        case 1:
                            editContactForImport(tableModel, presence, newContact);
                    }
                } else {
                    if (allNew) {
                        editContactForImport(tableModel, presence, newContact);
                    }
                }
            } else {
                tableModel.addContact(newContact);
            }
        }
    }

    /**
     * Меняет данные существующего контакта на импортируемые.
     *
     * @param tableModel Модель таблицы контактов.
     * @param presence   Индекс существующего контакта в таблице.
     * @param newContact Импортируемый контакт.
     */
    public static void editContactForImport(ContactTableModel tableModel, int presence, Contact newContact) {
        tableModel.getContacts().get(presence).updateContactImportInfo(newContact);
        tableModel.editContact(tableModel.getContacts().get(presence));
    }
}
