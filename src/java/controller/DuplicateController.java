/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package controller;

import model.Contact;
import model.ContactTableModel;
import model.Validator;
import view.MessageDialogs;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.time.LocalDate;
import java.util.List;

public class DuplicateController {
    /**
     * Проверяет наличие создаваемого/редактируемого в таблице и решает ситуацию совпадения.
     *
     * @param parent     Окно-родитель.
     * @param tableModel Модель таблицы контактов.
     * @param textFields Поля ввода.
     * @param date       Поле выбора даты.
     * @return 1 если контакт присутсвовал в таблице, 0 - если нет.
     */
    public static int checkAndSolveDuplicateFormContact(JDialog parent, ContactTableModel tableModel,
                                                        List<JTextComponent> textFields, LocalDate date) {
        Contact newContact = new Contact(textFields.get(0).getText(), textFields.get(1).getText(),
                textFields.get(2).getText());

        int presenceIndex = Validator.checkPresence(tableModel, newContact);

        if (presenceIndex != -1) {
            if (MessageDialogs.showSameContactDialog(parent) == 1) {
                JOptionPane.showMessageDialog(parent, "Данные сохранены!");
                tableModel.getAllContacts().get(presenceIndex).updateContactInfo(textFields, date);
                if (tableModel.getContacts().contains(newContact)) {
                    tableModel.editContact(tableModel.getAllContacts().get(presenceIndex));
                } else {
                    tableModel.fireTableDataChanged();
                }

                parent.dispose();
            }

            // Остаться в режиме редактирования / был дубликат == сработало.
            return 1;
        }

        // Дубликата не было.
        return 0;
    }
}
