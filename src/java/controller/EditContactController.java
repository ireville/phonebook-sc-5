/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package controller;

import model.Contact;
import model.ContactTableModel;
import model.ValidationError;
import model.Validator;
import org.jdatepicker.impl.JDatePickerImpl;
import view.EditContactDialog;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;

public class EditContactController {
    /**
     * Реакция на нажатие кнопки редактирования контакта: создаёт окно редактирования контакта.
     *
     * @param parent     Окно-родитель.
     * @param table      Таблица контактов.
     * @param tableModel Модель таблицы контактов.
     * @return Реакция на событие.
     */
    public static ActionListener createEditFormButtonListener(JFrame parent, JTable table, ContactTableModel tableModel) {
        return e -> {
            if (table.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(parent, "Вы не выбрали строку!", "Ошибка редактирования",
                        JOptionPane.ERROR_MESSAGE);
            } else {
                JDialog editFrame = new EditContactDialog(parent, tableModel,
                        tableModel.getContact(table.getSelectedRow()), "Edit");
                editFrame.setVisible(true);
            }
        };
    }

    /**
     * Реакция на нажатие кнопки "Сохранить" формы редактирования контакта.
     *
     * @param parent        Окно-родитель.
     * @param tableModel    Модель таблицы контактов.
     * @param editedContact Редактируемый контакт.
     * @param textFields    Поля ввода.
     * @param datePicker    Поле выбора даты.
     * @return Реакция на событие.
     */
    public static ActionListener saveEditFormButtonListener(JDialog parent, ContactTableModel tableModel,
                                                            Contact editedContact, List<JTextComponent> textFields,
                                                            JDatePickerImpl datePicker) {
        return e -> {
            LocalDate date = fillDate(datePicker);

            ValidationError error = Validator.validateForm(textFields, date);
            changeErrorFields(error, textFields);

            if (!error.isValid()) {
                JOptionPane.showMessageDialog(parent, error.getMessage(), "Ошибка редактирования",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }

            if (DuplicateController.checkAndSolveDuplicateFormContact(parent, tableModel, textFields, date) == 1) {
                return;
            }

            JOptionPane.showMessageDialog(parent, "Данные сохранены!");
            editedContact.updateContactInfo(textFields, date);
            tableModel.editContact(editedContact);
            parent.dispose();
        };
    }

    /**
     * Реакция на нажатие кнопки "Отмена" формы редактирования контакта: закрытие формы редактирования.
     *
     * @param currentDialog Текущее окно.
     * @return Реакция на событие.
     */
    public static ActionListener cancelEditFormButtonListener(JDialog currentDialog) {
        return e -> currentDialog.dispose();
    }

    /**
     * Меняет цвет рамки и текста ошибочных полей на красный.
     *
     * @param error      Ошибка валидации.
     * @param textFields Текстовые поля.
     */
    public static void changeErrorFields(ValidationError error, List<JTextComponent> textFields) {
        for (var field : textFields) {
            if (error.getErrorFields().contains(field)) {
                field.setForeground(Color.RED);
                field.setBorder(new LineBorder(Color.RED, 1));
            } else {
                field.setForeground(Color.BLACK);
                field.setBorder(new JTextField().getBorder());
            }
        }
    }

    /**
     * Берёт значение даты из поля выбора даты формы или проставляет null, если дата не была выбрана.
     *
     * @param datePicker Поля выбора даты.
     * @return Значение даты.
     */
    public static LocalDate fillDate(JDatePickerImpl datePicker) {
        LocalDate date = null;
        if (datePicker.getModel().getValue() != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            date = LocalDate.parse(sdf.format(datePicker.getModel().getValue()));
        }

        return date;
    }
}
