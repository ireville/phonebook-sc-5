/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package controller;

import com.opencsv.exceptions.CsvValidationException;
import model.ContactTableModel;
import model.FileWorker;
import model.Validator;
import view.MessageDialogs;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

public class FileWorkController {
    /**
     * Реакция на событие экспорта контактов: выбор файла и запись в него.
     *
     * @param parent     Компонент-родитель.
     * @param tableModel Модель таблицы контактов.
     * @return Реакция на событие.
     */
    public static ActionListener exportFileListener(Component parent, ContactTableModel tableModel) {
        return e -> {
            JFileChooser fileChooser = new JFileChooser();

            // Выбор файла для экспорта.
            if (fileChooser.showSaveDialog((JComponent) e.getSource()) == JFileChooser.APPROVE_OPTION) {
                // Предупреждение, если формат файла не .csv.
                if (!fileChooser.getSelectedFile().getName().endsWith(".csv")) {
                    if (MessageDialogs.showWrongFileFormatDialog((JComponent) e.getSource()) == 0) {
                        return;
                    }
                }

                try {
                    if (fileChooser.getSelectedFile().exists()) {
                        // Предупреждение перезаписи файла.
                        if (MessageDialogs.showFileEnsureDialog((JComponent) e.getSource(),
                                fileChooser.getSelectedFile()) == 0) {
                            return;
                        }
                    }

                    // Запись в файл.
                    FileWorker.writeTableToFile(tableModel, fileChooser.getSelectedFile());
                    JOptionPane.showMessageDialog(parent, "Данные сохранены!");

                } catch (IOException | SecurityException exception) {
                    MessageDialogs.showExportErrorDialog((JComponent) e.getSource(), fileChooser.getSelectedFile());
                }
            }
        };
    }

    /**
     * Реакция на событие импорта контактов: выбор файла и чтение из него.
     *
     * @param parent     Компонент-родитель.
     * @param tableModel Модель таблицы контактов.
     * @return Реакция на событие.
     */
    public static ActionListener importFileListener(Component parent, ContactTableModel tableModel) {
        return e -> {
            JFileChooser fileChooser = new JFileChooser();

            // Выбор файла для импорта.
            if (fileChooser.showOpenDialog((JComponent) e.getSource()) == JFileChooser.APPROVE_OPTION) {
                // Предупреждение, если формат файла не .csv.
                if (!fileChooser.getSelectedFile().getName().endsWith(".csv")) {
                    if (MessageDialogs.showWrongFileFormatDialog((JComponent) e.getSource()) == 0) {
                        return;
                    }
                }

                List<String[]> lineList;

                // Пытаемся считать данные.
                try {
                    lineList = FileWorker.readTableFromFile(fileChooser.getSelectedFile());
                } catch (CsvValidationException exception) {
                    MessageDialogs.showImportFormatErrorDialog((JComponent) e.getSource(), fileChooser.getSelectedFile());
                    return;
                } catch (IOException exception) {
                    MessageDialogs.showImportFileErrorDialog((JComponent) e.getSource(), fileChooser.getSelectedFile());
                    return;
                }

                // Проверяем корректность данных как контактов.
                for (String[] line : lineList) {
                    if (!Validator.validateContactLine(line)) {
                        MessageDialogs.showIncorrectImportTableDialog((JComponent) e.getSource());
                        return;
                    }
                }

                // Добавляем считанные контакты.
                AddContactController.organizeContactImport(parent, tableModel, lineList);
            }
        };
    }
}
