/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package controller;

import model.ContactTableModel;
import view.MessageDialogs;

import javax.swing.*;
import java.awt.event.ActionListener;

public class RemoveContactController {
    /**
     * Реакция на нажатие кнопки удаления контакта: удаляет контакт после подтверждения действия.
     *
     * @param parent     Окно-родитель.
     * @param table      Таблица контактов.
     * @param tableModel Модель таблицы контактов.
     * @return Реакция на событие.
     */
    public static ActionListener removeContactButtonListener(JFrame parent, JTable table, ContactTableModel tableModel) {
        return e -> {
            if (table.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(parent, "Вы не выбрали строку!", "Ошибка удаления",
                        JOptionPane.ERROR_MESSAGE);
            } else {
                if (MessageDialogs.showEnsureRemoveContactDialog(parent) == 1) {
                    tableModel.removeContact(tableModel.getContact(table.getSelectedRow()));
                    JOptionPane.showMessageDialog(parent, "Контакт удалён.");
                }
            }
        };
    }
}
