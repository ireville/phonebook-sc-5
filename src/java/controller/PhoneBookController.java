/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package controller;

import model.ContactTableModel;
import model.FileWorker;
import view.MessageDialogs;
import view.PhoneBookFrame;

import javax.swing.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;

public class PhoneBookController {
    /**
     * Реакция на нажатие кнопки "ENTER" при поиске: фильтрация таблицы.
     *
     * @param tableModel Модель таблицы контактов.
     * @return Реакция на событие.
     */
    public static KeyListener searchLineListener(ContactTableModel tableModel) {
        return new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    tableModel.setFilter(((JTextField) e.getSource()).getText());

                    // Делаем строку поиска вновь пустой для удобства последующих запросов.
                    ((JTextField) e.getSource()).setText("");
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        };
    }

    /**
     * Реакция на нажатие кнопки "Поиск" при поиске: фильтрация таблицы.
     *
     * @param tableModel Модель таблицы контактов.
     * @return Реакция на событие.
     */
    public static ActionListener searchButtonListener(ContactTableModel tableModel, JTextField searchLine) {
        return e -> {
            tableModel.setFilter(searchLine.getText());

            // Делаем строку поиска вновь пустой для удобства последующих запросов.
            searchLine.setText("");
        };
    }

    /**
     * Реакция на нажатие элемента меню "О разработчике".
     *
     * @param parent Окно-родитель.
     * @return Реакция на событие.
     */
    public static ActionListener aboutButtonListener(JFrame parent) {
        return e -> MessageDialogs.showMyInfoDialog(parent);
    }

    /**
     * Реакция на нажатие кнопки закрытия главного окна.
     *
     * @param frame Закрываемое окно.
     * @return Реакция на событие.
     */
    public static ActionListener closeFrameButtonListener(PhoneBookFrame frame) {
        return e -> frame.close();
    }

    /**
     * Реакция на закрытие главного окна.
     *
     * @param parent     Окно.
     * @param tableModel Модель таблицы контактов.
     * @param filePath   Путь до файла для сохранения таблицы.
     * @return Реакция на событие.
     */
    public static WindowAdapter closeFrameListener(JFrame parent, ContactTableModel tableModel, String filePath) {
        return new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    if (tableModel.getContacts().size() != 0) {
                        savePhoneBook(parent, tableModel, filePath);
                    }
                    else{
                        JOptionPane.showMessageDialog(parent, "Отображаемая телефонная книга пуста, так что " +
                                "сохраняться не будет. ");
                    }
                } catch (IOException ex) {
                    MessageDialogs.showSaveErrorDialog(parent);
                }
                super.windowClosing(e);
            }
        };
    }

    /**
     * Сохраняет телефонную книгу в файл.
     *
     * @param parent     Окно-родитель.
     * @param tableModel Модель таблицы контактов.
     * @param filePath   Путь до файла для сохранения таблицы.
     * @throws IOException Если возникает ошибка записи в файл по заданному пути.
     */
    public static void savePhoneBook(JFrame parent, ContactTableModel tableModel, String filePath) throws IOException {
        FileWorker.writeTableToFile(tableModel, new File(filePath));
        JOptionPane.showMessageDialog(parent, "Данные Вашей телефонной книги сохранены в файл " + filePath);
    }
}
