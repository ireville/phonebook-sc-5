/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package model;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;

public class FileWorker {
    /**
     * Записывает в файл телефонную книгу в формате .csv.
     *
     * @param tableModel Модель таблицы контактов.
     * @param file       Файл для записи.
     * @throws IOException Выбрасывается в случае ошибки записи в файл.
     */
    public static void writeTableToFile(ContactTableModel tableModel, File file) throws IOException, SecurityException {
        CSVWriter writer = new CSVWriter(new FileWriter(file.getAbsolutePath()));

        for (Contact contact : tableModel.getContacts()) {
            writer.writeNext(contact.getFieldsAsStrings());
        }

        writer.close();
    }

    /**
     * Считывает из файла телефонную книгу. Предполагаемый формат записи таблицы - .csv.
     *
     * @param file Файл для чтения.
     * @return Список строк телефонной книги.
     * @throws IOException            Выбрасывается в случае ошибки чтения из файла.
     * @throws CsvValidationException Выбрасывается в случае ошибочного (не .csv) формата таблицы.
     */
    public static List<String[]> readTableFromFile(File file) throws IOException, CsvValidationException {
        Reader reader = new FileReader(file);
        CSVReader csvReader = new CSVReader(reader);

        List<String[]> linesList = new ArrayList<>();
        String[] line;
        while ((line = csvReader.readNext()) != null) {
            linesList.add(line);
        }

        reader.close();
        csvReader.close();

        return linesList;
    }
}
