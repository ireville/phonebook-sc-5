/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package model;

import javax.swing.text.JTextComponent;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Contact {
    private String surname = "";
    private String name = "";
    private String patronymic = "";
    private String mobilePhone = "";
    private String homePhone = "";
    private String address = "";
    private LocalDate birthDate;
    private String notes = "";

    public Contact(String surname, String name, String patronymic, String mobilePhone, String homePhone,
                   String address, LocalDate birthDate, String notes) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.mobilePhone = mobilePhone;
        this.homePhone = homePhone;
        this.address = address;
        this.birthDate = birthDate;
        this.notes = notes;
    }

    public Contact(String surname, String name, String patronymic) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
    }

    public Contact() {
    }

    /**
     * Создает контакт по строковому представлению каждого из полей.
     *
     * @param line Строковое представление полей.
     */
    public Contact(String[] line) {
        this.surname = line[0];
        this.name = line[1];
        this.patronymic = line[2];
        this.mobilePhone = line[3];
        this.homePhone = line[4];
        this.address = line[5];
        this.birthDate = (line[6].equals("") ? null : LocalDate.parse(line[6]));
        this.notes = line[7];
    }

    /**
     * Возвращает названия полей контакта.
     *
     * @return Названия полей.
     */
    public static List<String> getFieldNames() {
        return List.of("Фамилия", "Имя", "Отчество", "Мобильный телефон",
                "Домашний телефон", "Адрес", "День Рождения", "Комментарий");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Contact)) {
            return false;
        }
        Contact contact = (Contact) o;
        return Objects.equals(getSurname(), contact.getSurname()) &&
                Objects.equals(getName(), contact.getName()) &&
                Objects.equals(getPatronymic(), contact.getPatronymic());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSurname(), getName(), getPatronymic());
    }

    /**
     * Вовзращает поля контакта.
     *
     * @return Поля контакта.
     */
    public Object[] getFields() {
        return new Object[]{
                this.surname,
                this.name,
                this.patronymic,
                this.mobilePhone,
                this.homePhone,
                this.address,
                this.birthDate,
                this.notes
        };
    }

    /**
     * Возвращает строковое представление полей контакта.
     *
     * @return Массив строк-полей.
     */
    public String[] getFieldsAsStrings() {
        return Arrays.stream(getFields()).map(object -> {
            if (object == null) {
                return "";
            } else {
                return object.toString();
            }
        }).toArray(String[]::new);
    }

    /**
     * Обновляет данные существующего контакта на введённые.
     *
     * @param textFields Поля ввода.
     * @param date       Выбранная дата.
     */
    public void updateContactInfo(List<JTextComponent> textFields, LocalDate date) {
        setSurname(textFields.get(0).getText());
        setName(textFields.get(1).getText());
        setPatronymic(textFields.get(2).getText());
        setMobilePhone(textFields.get(3).getText());
        setHomePhone(textFields.get(4).getText());
        setAddress(textFields.get(5).getText());
        setBirthDate(date);
        setNotes(textFields.get(6).getText());
    }

    /**
     * Обновляет данные существующего контакта на импортируемые.
     *
     * @param newContact Новые данные контакта из импорта.
     */
    public void updateContactImportInfo(Contact newContact) {
        setMobilePhone(newContact.getMobilePhone());
        setHomePhone(newContact.getHomePhone());
        setAddress(newContact.getAddress());
        setBirthDate(newContact.getBirthDate());
        setNotes(newContact.getNotes());
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
