/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package model;

import view.PhoneBookTable;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ContactTableModel extends AbstractTableModel {
    private final List<Contact> contacts = new ArrayList<>();

    // Текущий применённый к таблице фильтр.
    private String filter;

    // Отфильтрованнный список контактов.
    private List<Contact> filteredContacts;

    public ContactTableModel() {
        // Изначально фильтр пустой.
        this.filter = "";
        filteredContacts = filterContacts(filter);
    }

    // Количество строк.
    @Override
    public int getRowCount() {
        return filteredContacts.size();
    }

    // Количество столбцов.
    @Override
    public int getColumnCount() {
        return Contact.getFieldNames().size();
    }

    @Override
    public String getColumnName(int column) {
        return PhoneBookTable.getColumnNames().get(column);
    }

    // Функция определения данных ячейки.
    @Override
    public Object getValueAt(int row, int column) {
        return filteredContacts.get(row).getFields()[column];
    }

    /**
     * Возвращает контакт, находящийся в указанной строке.
     *
     * @param row Номер строки.
     * @return Найденный контакт.
     */
    public Contact getContact(int row) {
        return filteredContacts.get(row);
    }

    /**
     * Возвращает список контактов, удовлетворяющий фильтру. Если фильтр пустой - список всех контактов.
     *
     * @return Список контактов.
     */
    public List<Contact> getContacts() {
        return filterContacts(filter);
    }

    /**
     * Возвращает список всех онтактов.
     *
     * @return Список всех контактов.
     */
    public List<Contact> getAllContacts() {
        return contacts;
    }

    /**
     * Добавляет контакт в таблицу.
     *
     * @param contact Контакт.
     */
    public void addContact(Contact contact) {
        contacts.add(contact);
        filteredContacts = filterContacts(filter);
        fireTableRowsInserted(filteredContacts.size() - 1, filteredContacts.size() - 1);
    }

    /**
     * Обновляет данные о контакте в таблице.
     *
     * @param contact Контакт.
     */
    public void editContact(Contact contact) {
        fireTableRowsUpdated(filteredContacts.indexOf(contact), filteredContacts.indexOf(contact));
        filteredContacts = filterContacts(filter);
    }

    /**
     * Удаляет данные о контакте из таблицы.
     *
     * @param contact Контакт.
     */
    public void removeContact(Contact contact) {
        contacts.remove(contact);
        filteredContacts = filterContacts(filter);
        fireTableRowsDeleted(filteredContacts.indexOf(contact), filteredContacts.indexOf(contact));
    }

    // Является ли ячейка изменяемой.
    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    /**
     * Фильтрует список контактов.
     *
     * @param filter Фильтр.
     * @return Отфильтрованный список контактов.
     */
    private List<Contact> filterContacts(String filter) {
        return contacts.stream().filter(contact -> contact.getSurname().contains(filter) ||
                contact.getName().contains(filter) || contact.getPatronymic().contains(filter))
                .collect(Collectors.toList());
    }

    /**
     * Задаёт новый фильтр.
     *
     * @param filter Новый фильтр.
     */
    public void setFilter(String filter) {
        // Если фильтр поменялся, то только тогда его запоминаем, заново фильтруем список контактов и обновялем таблицу.
        if (!this.filter.equals(filter)) {
            this.filter = filter;
            this.filteredContacts = filterContacts(filter);
            fireTableDataChanged();
        }
    }
}
