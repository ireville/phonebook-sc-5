/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package model;

import javax.swing.text.JTextComponent;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ValidationError {
    // Маркер валидности. Если истина, то ошибки не были найдены, иначе - ложь.
    private boolean isValid;

    // Список некорректных текстовых полей.
    private final List<JTextComponent> errorFields;

    // Сообщение о последней ошибке.
    private String message;

    public ValidationError() {
        // Изначально считаем, что все ок.
        this.isValid = true;
        this.errorFields = new ArrayList<>();
    }

    /**
     * Добавляет поле в число некорректных.
     *
     * @param textField Текстовое поле.
     */
    public void addError(JTextComponent textField) {
        if (textField != null) {
            setValid(false);
            errorFields.add(textField);
        }
    }

    /**
     * Добавляет список полей в число некорректных.
     *
     * @param textFields Текстовые поля.
     */
    public void addErrors(List<JTextComponent> textFields) {
        if (textFields != null) {
            setValid(false);
            errorFields.addAll(textFields.stream().filter(Objects::nonNull).collect(Collectors.toList()));
        }
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<JTextComponent> getErrorFields() {
        return errorFields;
    }
}
