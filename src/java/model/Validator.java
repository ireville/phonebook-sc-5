/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package model;

import javax.swing.text.JTextComponent;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    /**
     * Валидация телефона.
     *
     * @param phone Телефон.
     * @return Истину, если телефон пустой или состоит только из цифр, ложь - иначе.
     */
    public static boolean validateTelephone(String phone) {
        if (phone.isBlank()) {
            return true;
        }

        Pattern pattern = Pattern.compile("^([0-9]*)$");
        Matcher matcher = pattern.matcher(phone);

        return matcher.matches();
    }

    /**
     * Валидация даты рождения: считается некорректной, если принадлежит моменту в будущем.
     *
     * @param date Дата рождения.
     * @return Истину, если корректна, ложь - иначе.
     */
    public static boolean validateDate(LocalDate date) {
        return (date == null || date.isBefore(LocalDate.now()) || date.isEqual(LocalDate.now()));
    }

    /**
     * Валидация формы.
     *
     * @param textFields Текстовые поля формы.
     * @param date       Выбранная дата рождения.
     * @return Ошибку валидации, хранящую необходимую информацию.
     */
    public static ValidationError validateForm(List<JTextComponent> textFields, LocalDate date) {
        ValidationError error = new ValidationError();

        // Проверка на присутствие нужной комбинации полей.
        if (textFields.get(0).getText().isBlank()) {
            error.addError(textFields.get(0));
            error.setMessage("Фимилия не может быть пустой.");
        }

        if (textFields.get(1).getText().isBlank()) {
            error.addError(textFields.get(1));
            error.setMessage("Имя не может быть пустым.");
        }

        if (textFields.get(3).getText().isBlank() && textFields.get(4).getText().isBlank()) {
            error.addErrors(List.of(textFields.get(3), textFields.get(4)));

            error.setMessage("Оба телефона не могут быть одновременно пустыми.");
        } else {
            // Проверка телефонов.
            if (!validateTelephone(textFields.get(3).getText())) {
                error.addError(textFields.get(3));
                error.setMessage("Некорректный мобильный телефон.");
            }

            if (!validateTelephone(textFields.get(4).getText())) {
                error.addError(textFields.get(4));
                error.setMessage("Некорректный домашний телефон.");
            }
        }

        // Проверка даты рождения.
        if (!validateDate(date)) {
            error.setValid(false);
            error.setMessage("Рождение в будущем невозможно.\nНекорректная дата.");
        }

        for (JTextComponent field : textFields) {
            if (field.getText().contains("<")) {
                error.addError(field);
                error.setMessage("Ни одно поле не может содержать символ '<'.");
            }
        }

        return error;
    }

    /**
     * Валидация корректности строки, представляющей контакт.
     *
     * @param line Список строк, представляющих контакт.
     * @return Истину, если список корректен, ложь - иначе.
     */
    public static boolean validateContactLine(String[] line) {
        // Если строка пустая, содержит пустые элементы или недостаточное их количество.
        if (line == null || line.length < Contact.getFieldNames().size() ||
                Arrays.stream(line).anyMatch(str -> str == null || str.contains("<"))) {
            return false;
        }

        // Проверка на присутствие нужной комбинации полей.
        if (line[0].isBlank() || line[1].isBlank() || (line[3].isBlank() && line[4].isBlank())) {
            return false;
        }

        // Проверка телефонов.
        if (!validateTelephone(line[3]) || !validateTelephone(line[4])) {
            return false;
        }

        // Проверка даты рождения.
        try {
            if (!line[6].equals("")) {
                LocalDate date = LocalDate.parse(line[6]);
                return (date.isBefore(LocalDate.now()) || date.isEqual(LocalDate.now()));
            }
        } catch (DateTimeParseException exception) {
            return false;
        }

        return true;
    }

    /**
     * Проверка наличия контакта в таблице.
     *
     * @param tableModel Модель таблицы контактов.
     * @param newContact Контакт.
     * @return Индекс присутсвия в таблице или -1 при отсутствии в таблице.
     */
    public static int checkPresence(ContactTableModel tableModel, Contact newContact) {
        return tableModel.getAllContacts().indexOf(newContact);
    }
}
